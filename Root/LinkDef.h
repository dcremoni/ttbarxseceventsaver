#include "TTbarXsecEventSaver/DileptonSaver.h"
#include "TTbarXsecEventSaver/SingleLepSaver.h"

#ifdef __CINT__
#pragma extra_include "TTbarXsecEventSaver/DileptonSaver.h";
#pragma extra_include "TTbarXsecEventSaver/SingleLepSaver.h";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//for loading the object selection at run time
#pragma link C++ class top::TTbarXsecEventSaver::DileptonSaver+;
#pragma link C++ class top::TTbarXsecEventSaver::SingleLepSaver+;

#endif
