#include "TTbarXsecEventSaver/SingleLepSaver.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "TFile.h"
#include "Math/Vector4D.h"

// needed for std::next_permutation
#include <algorithm>

namespace top {
  namespace TTbarXsecEventSaver {
    ///-- Constructor --///
    SingleLepSaver::SingleLepSaver() :
    m_config(nullptr),
    m_isMC(true),
    m_dsid(-1),
    m_hadBjetIndex(999),
    m_lightJetIndex1(999),
    m_lightJetIndex2(999),
    m_chi2(99999.),
    m_electronIdSf(1.),
    m_electronRecoSf(1.),
    m_electronIsoSf(1.),
    m_muonIdSf(1.),
    m_muonIsoSf(1.),
    m_muonTTVASf(1.)
    {
    }
    
    ///-- initialize - done once at the start of a job before the loop over events --///
    void SingleLepSaver::initialize(std::shared_ptr<TopConfig> config,
                                    TFile* file,
                                    const std::vector<std::string>& extraBranches) {
      ///-- Let the base class do all the hard work --///
      ///-- It will setup TTrees for each systematic with a standard set of variables --///
      EventSaverFlatNtuple::initialize(config, file, extraBranches);
     
      m_config = config;
      m_isMC = m_config->isMC(); 
      m_dsid = m_config->getDSID();
      ///-- Loop over the systematic TTrees and add the custom variables --///
    
    
      for (auto systematicTree : treeManagers()) {
        // Run for everything
        systematicTree->makeOutputVariable(m_hadBjetIndex, "hadBjetIndex");
        systematicTree->makeOutputVariable(m_lightJetIndex1, "lightJetIndex1");
        systematicTree->makeOutputVariable(m_lightJetIndex2, "lightJetIndex2");
        systematicTree->makeOutputVariable(m_chi2, "chi2");
        systematicTree->makeOutputVariable(m_truthJetFlavour, "truthJetFlavour");
        if (m_isMC && systematicTree->name() == "nominal") {
          systematicTree->makeOutputVariable(m_electronIdSf, "electronIdSf");
          systematicTree->makeOutputVariable(m_electronRecoSf, "electronRecoSf");
          systematicTree->makeOutputVariable(m_electronIsoSf, "electronIsoSf");
          systematicTree->makeOutputVariable(m_muonIdSf, "muonIdSf");
          systematicTree->makeOutputVariable(m_muonTTVASf, "muonTTVASf");
          systematicTree->makeOutputVariable(m_muonIsoSf, "muonIsoSf");
        }
      }
    }
    
    ///-- saveEvent - run for every systematic and every event --///
    void SingleLepSaver::saveEvent(const top::Event& event) {
      m_dsid = -1;
      m_hadBjetIndex = 999;
      m_lightJetIndex1 = 999;
      m_lightJetIndex2 = 999;
      m_chi2 = 999999.;
      m_truthJetFlavour.clear();

      // We want to run only on events that pass selection
      if(!event.m_saveEvent && m_config->saveOnlySelectedEvents()) return; 

      this->matchJets(event); 

      // save the truth jet flavour
      if (m_isMC) {
        // get jet truth flavor
        const xAOD::JetContainer* truthJets = nullptr;
        top::check(evtStore()->retrieve(truthJets, m_config->sgKeyTruthJets()), "FAILURE");
        for (const auto ijet : *truthJets) {
          if (ijet->isAvailable<int>("HadronConeExclTruthLabelID")) {
            int flavourlabel(0);
            ijet->getAttribute("HadronConeExclTruthLabelID", flavourlabel);
            m_truthJetFlavour.emplace_back(flavourlabel);
          }
        }

        // get the individual SFs
        float eleId(1.);
        float eleReco(1.);
        float eleIso(1.);

        for (const auto* iele : event.m_electrons) {
          eleId   *= iele->isAvailable<float>("EL_SF_ID_"+m_config->electronID()) ? iele->auxdataConst<float>("EL_SF_ID_"+m_config->electronID()) : 1.0;
          eleReco *= iele->isAvailable<float>("EL_SF_Reco") ? iele->auxdataConst<float>("EL_SF_Reco") : 1.;
          eleIso  *= iele->isAvailable<float>("EL_SF_Iso_"+m_config->electronIsolationSF()) ? iele->auxdataConst<float>("EL_SF_Iso_"+m_config->electronIsolationSF()) : 1.0;
        }

        m_electronIdSf   = eleId;
        m_electronRecoSf = eleReco;
        m_electronIsoSf  = eleIso;
        
        float muId(1.);
        float muTTVA(1.);
        float muIso(1.);

        for (const auto* imu : event.m_muons) {
          muId   *= imu->isAvailable<float>("MU_SF_ID_"+m_config->muonQuality()) ? imu->auxdataConst<float>("MU_SF_ID_"+m_config->muonQuality()) : 1.;
          muTTVA *= imu->isAvailable<float>("MU_SF_TTVA") ? imu->auxdataConst<float>("MU_SF_TTVA") : 1.0;
          muIso  *= imu->isAvailable<float>("MU_SF_Isol_"+m_config->muonIsolationSF()) ? imu->auxdataConst<float>("MU_SF_Isol_"+m_config->muonIsolationSF()) : 1.;
        }

        m_muonIdSf   = muId;
        m_muonTTVASf = muTTVA;
        m_muonIsoSf  = muIso;
      }

      ///-- Let the base class do all the hard work --///
      EventSaverFlatNtuple::saveEvent(event);
    }
      
    void SingleLepSaver::matchJets(const Event& event) {
      std::vector<int> bIndices{};
      std::vector<int> lIndices{};

      // get a list of btag and light jet indices
      for (std::size_t i = 0; i < event.m_jets.size(); ++i) {
        if (event.m_jets.at(i)->auxdataConst<char>("isbtagged_DL1dv01_FixedCutBEff_77")) {
          if (bIndices.size() < 2) {
            bIndices.emplace_back(i);
          }
        } else {
          if (lIndices.size() < 4) {
            lIndices.emplace_back(i);
          }
        }
      }

      if (bIndices.empty()) {
        return;
      }

      if (lIndices.size() < 2) {
        return;
      }

      // oly consider 3 light jets when there are 2 b jets
      if (bIndices.size() == 2) {
        if (lIndices.size() > 3) {
          lIndices.resize(3);
        }
      }

      std::vector<int> lIndicesOrig = lIndices;
      // original b jet index and light jet indices
      this->runSinglePermutation(event, bIndices.at(0), lIndices.at(0), lIndices.at(1));
     

      // run light jet permutations
      while (std::next_permutation(lIndices.begin(), lIndices.end())) {

        // effectively the same
        if (m_lightJetIndex1 == lIndices.at(0) &&
            m_lightJetIndex2 == lIndices.at(1)) continue;
        if (m_lightJetIndex1 == lIndices.at(1) &&
            m_lightJetIndex2 == lIndices.at(0)) continue;

        this->runSinglePermutation(event, bIndices.at(0), lIndices.at(0), lIndices.at(1));
      }

      // now run the permutations on b_jets
      while (std::next_permutation(bIndices.begin(), bIndices.end())) {

        // original light jets
        this->runSinglePermutation(event, bIndices.at(0), lIndicesOrig.at(0), lIndicesOrig.at(1));

        // permutation of light jets
        while (std::next_permutation(lIndicesOrig.begin(), lIndicesOrig.end())) {

          // effectively the same
          if (m_hadBjetIndex == bIndices.at(0) &&
              m_lightJetIndex1 == lIndicesOrig.at(0) &&
              m_lightJetIndex2 == lIndicesOrig.at(1)) continue;
          if (m_hadBjetIndex == bIndices.at(0) &&
              m_lightJetIndex1 == lIndicesOrig.at(1) &&
              m_lightJetIndex2 == lIndicesOrig.at(0)) continue;

          this->runSinglePermutation(event, bIndices.at(0), lIndicesOrig.at(0), lIndicesOrig.at(1));
        }
      }
    }

    float SingleLepSaver::chi2(const float recoWmass, const float recoTopMass) {
      static constexpr float massW = 80.38;
      static constexpr float massTop = 172.5;
      static constexpr float widthW = 25;
      static constexpr float widthTop = 35;

      const float a = (recoWmass - massW)*(recoWmass - massW)/(widthW*widthW);
      const float b = (recoTopMass - massTop)*(recoTopMass - massTop)/(widthTop*widthTop);

      return a + b;
    }  
      
    void SingleLepSaver::runSinglePermutation(const top::Event& event, const int bIndex, const int lIndex1, const int lIndex2) {
      ROOT::Math::PtEtaPhiEVector bHad(event.m_jets.at(bIndex)->pt()/1e3,
                                       event.m_jets.at(bIndex)->eta(),
                                       event.m_jets.at(bIndex)->phi(),
                                       event.m_jets.at(bIndex)->e()/1e3);

      ROOT::Math::PtEtaPhiEVector l1(event.m_jets.at(lIndex1)->pt()/1e3,
                                     event.m_jets.at(lIndex1)->eta(),
                                     event.m_jets.at(lIndex1)->phi(),
                                     event.m_jets.at(lIndex1)->e()/1e3);

      ROOT::Math::PtEtaPhiEVector l2(event.m_jets.at(lIndex2)->pt()/1e3,
                                     event.m_jets.at(lIndex2)->eta(),
                                     event.m_jets.at(lIndex2)->phi(),
                                     event.m_jets.at(lIndex2)->e()/1e3);

      const float mW = (l1+l2).M();
      const float mT = (l1+l2+bHad).M();
      const float currentChi2 = SingleLepSaver::chi2(mW, mT);

      if (currentChi2 < m_chi2) {
        m_chi2 = currentChi2;
        m_hadBjetIndex = bIndex;
        m_lightJetIndex1 = lIndex1;
        m_lightJetIndex2 = lIndex2;
      }
    }
  }
}
