#include "TTbarXsecEventSaver/DileptonSaver.h"
#include "TopConfiguration/ConfigurationSettings.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "xAODBTagging/BTaggingUtilities.h"

#include "TFile.h"

namespace top {
  namespace TTbarXsecEventSaver {
    ///-- Constructor --///
    DileptonSaver::DileptonSaver() :
      m_config(nullptr),
      m_isMC(true),
      m_dsid(-1),
    
      m_ee_Mass(-1000.),
      m_ee_DeltaR(-1.),
      m_ee_DeltaPhi(-999.),
      m_ee_Z_pt(-100),
      m_ee_lead_pt(-100),
      m_ee_subl_pt(-100),
      m_ee_lead_eta(-100),
      m_ee_subl_eta(-100),
      m_ee_lead_phi(-100),
      m_ee_subl_phi(-100),

      m_mumu_Mass(-1000.),
      m_mumu_DeltaR(-1.),
      m_mumu_DeltaPhi(-999.),
      m_mumu_Z_pt(-100),
      m_mumu_lead_pt(-100),
      m_mumu_subl_pt(-100),
      m_mumu_lead_eta(-100),
      m_mumu_subl_eta(-100),
      m_mumu_lead_phi(-100),
      m_mumu_subl_phi(-100),
    
      m_electronIdSf(1.),
      m_electronRecoSf(1.),
      m_electronIsoSf(1.),
    
      m_muonIdSf(1.),
      m_muonIsoSf(1.),
      m_muonTTVASf(1.),
    
      m_storeFtag(false)
    {
    }
    
    ///-- initialize - done once at the start of a job before the loop over events --///
    void DileptonSaver::initialize(std::shared_ptr<TopConfig> config,
                                   TFile* file,
                                   const std::vector<std::string>& extraBranches) {
      ///-- Let the base class do all the hard work --///
      ///-- It will setup TTrees for each systematic with a standard set of variables --///
      EventSaverFlatNtuple::initialize(config, file, extraBranches);
     
      m_config = config;
      m_isMC = m_config->isMC(); 
      ATH_MSG_INFO("Is MC " << m_isMC);
      m_dsid = m_config->getDSID();
      m_configSettings = top::ConfigurationSettings::get();
      if (m_configSettings->value("StoreFTAG") == "True") m_storeFtag = true;
      ///-- Loop over the systematic TTrees and add the custom variables --///
    
    
      for (auto systematicTree : treeManagers()) {
        // Run for everything
	systematicTree->makeOutputVariable(m_ee_Mass, "ee_Mass");
	systematicTree->makeOutputVariable(m_ee_DeltaR, "ee_DeltaR");
	systematicTree->makeOutputVariable(m_ee_DeltaPhi, "ee_DeltaPhi");
	systematicTree->makeOutputVariable(m_ee_Z_pt, "ee_Z_pt");
	systematicTree->makeOutputVariable(m_ee_lead_pt, "ee_lead_pt");
	systematicTree->makeOutputVariable(m_ee_subl_pt, "ee_subl_pt");
	systematicTree->makeOutputVariable(m_ee_lead_eta, "ee_lead_eta");
	systematicTree->makeOutputVariable(m_ee_subl_eta, "ee_subl_eta");
	systematicTree->makeOutputVariable(m_ee_lead_phi, "ee_lead_phi");
	systematicTree->makeOutputVariable(m_ee_subl_phi, "ee_subl_phi");

	systematicTree->makeOutputVariable(m_mumu_Mass, "mumu_Mass");
	systematicTree->makeOutputVariable(m_mumu_DeltaR, "mumu_DeltaR");
	systematicTree->makeOutputVariable(m_mumu_DeltaPhi, "mumu_DeltaPhi");
	systematicTree->makeOutputVariable(m_mumu_Z_pt, "mumu_Z_pt");
	systematicTree->makeOutputVariable(m_mumu_lead_pt, "mumu_lead_pt");
	systematicTree->makeOutputVariable(m_mumu_subl_pt, "mumu_subl_pt");
	systematicTree->makeOutputVariable(m_mumu_lead_eta, "mumu_lead_eta");
	systematicTree->makeOutputVariable(m_mumu_subl_eta, "mumu_subl_eta");
	systematicTree->makeOutputVariable(m_mumu_lead_phi, "mumu_lead_phi");
	systematicTree->makeOutputVariable(m_mumu_subl_phi, "mumu_subl_phi");
        
        if (m_isMC && systematicTree->name() == "nominal") {
          systematicTree->makeOutputVariable(m_electronIdSf, "electronIdSf");
          systematicTree->makeOutputVariable(m_electronRecoSf, "electronRecoSf");
          systematicTree->makeOutputVariable(m_electronIsoSf, "electronIsoSf");
          systematicTree->makeOutputVariable(m_muonIdSf, "muonIdSf");
          systematicTree->makeOutputVariable(m_muonTTVASf, "muonTTVASf");
          systematicTree->makeOutputVariable(m_muonIsoSf, "muonIsoSf");
        }
      }
    }
    
    ///-- saveEvent - run for every systematic and every event --///
    void DileptonSaver::saveEvent(const top::Event& event) {
      // We want to run only on events that pass selection
      if(!event.m_saveEvent && m_config->saveOnlySelectedEvents()) return; 

      /// reset variables
      /*
      m_dileptonMass = -1.;
      m_dileptonDeltaR = -1.;
      m_dileptonDeltaPhi = -999.;
      */
      m_ee_Mass = -1000.;
      m_ee_DeltaR = -1.;
      m_ee_DeltaPhi = -999.;
      m_ee_Z_pt = -100;
      m_ee_lead_pt = -100;
      m_ee_subl_pt = -100;
      m_ee_lead_eta = -100;
      m_ee_subl_eta = -100;
      m_ee_lead_phi = -100;
      m_ee_subl_phi = -100;

      m_mumu_Mass = -1000.;
      m_mumu_DeltaR = -1.;
      m_mumu_DeltaPhi = -999.;
      m_mumu_Z_pt = -100;
      m_mumu_lead_pt = -100;
      m_mumu_subl_pt = -100;
      m_mumu_lead_eta = -100;
      m_mumu_subl_eta = -100;
      m_mumu_lead_phi = -100;
      m_mumu_subl_phi = -100;

      m_electronIdSf = 1.;
      m_electronRecoSf = 1.;
      m_electronIsoSf = 1.;
      m_muonIdSf = 1.;
      m_muonIsoSf = 1.;
      m_muonTTVASf = 1.;
      

      /// Calculate what we need
      TLorentzVector l1{};
      TLorentzVector l2{};
      int lead = 0, sub_lead = 1;
      //ATH_MSG_INFO("Number of electrons " << event.m_electrons.size());
      //ATH_MSG_INFO("Number of muons" << event.m_muons.size());
      if (event.m_electrons.size() == 2) {// 2 electrons
        l1.SetPtEtaPhiE(event.m_electrons.at(0)->pt(), event.m_electrons.at(0)->eta(), event.m_electrons.at(0)->phi(), event.m_electrons.at(0)->e());
        l2.SetPtEtaPhiE(event.m_electrons.at(1)->pt(), event.m_electrons.at(1)->eta(), event.m_electrons.at(1)->phi(), event.m_electrons.at(1)->e());
	m_ee_Mass     = (l1+l2).M();
	m_ee_DeltaR   = l1.DeltaR(l2);
	m_ee_DeltaPhi = l1.DeltaPhi(l2);
	m_ee_Z_pt = (l1+l2).Pt();
	if(event.m_electrons.at(0)->pt() > event.m_electrons.at(1)->pt()){
	  lead = 0;
	  sub_lead = 1;
	}else{
	  lead = 1;
	  sub_lead = 0;
	}
	m_ee_lead_pt = event.m_electrons.at(lead)->pt();
	m_ee_subl_pt = event.m_electrons.at(sub_lead)->pt();
	m_ee_lead_eta = event.m_electrons.at(lead)->eta();
	m_ee_subl_eta = event.m_electrons.at(sub_lead)->eta();
	m_ee_lead_phi = event.m_electrons.at(lead)->phi();
	m_ee_subl_phi = event.m_electrons.at(sub_lead)->phi();
	  
      } else if (event.m_muons.size() == 2) {// 2 muons
        l1.SetPtEtaPhiE(event.m_muons.at(0)->pt(), event.m_muons.at(0)->eta(), event.m_muons.at(0)->phi(), event.m_muons.at(0)->e());
        l2.SetPtEtaPhiE(event.m_muons.at(1)->pt(), event.m_muons.at(1)->eta(), event.m_muons.at(1)->phi(), event.m_muons.at(1)->e());
	m_mumu_Mass     = (l1+l2).M();
	m_mumu_DeltaR   = l1.DeltaR(l2);
	m_mumu_DeltaPhi = l1.DeltaPhi(l2);
	m_mumu_Z_pt = (l1+l2).Pt();

	if(event.m_muons.at(0)->pt() > event.m_muons.at(1)->pt()){
	  lead = 0;
	  sub_lead = 1;
	}else{
	  lead = 1;
	  sub_lead = 0;
	}
	m_mumu_lead_pt = event.m_muons.at(lead)->pt();
	m_mumu_subl_pt = event.m_muons.at(sub_lead)->pt();
	m_mumu_lead_eta = event.m_muons.at(lead)->eta();
	m_mumu_subl_eta = event.m_muons.at(sub_lead)->eta();
	m_mumu_lead_phi = event.m_muons.at(lead)->phi();
	m_mumu_subl_phi = event.m_muons.at(sub_lead)->phi();
      }  else {
        throw std::runtime_error{"DileptonSaver::saveEvent: Is not a dilepton event!"};
      }
      /*
      m_dileptonMass     = (l1+l2).M();
      m_dileptonDeltaR   = l1.DeltaR(l2);
      m_dileptonDeltaPhi = l1.DeltaPhi(l2);
      */
      if (m_isMC) {
        // get the individual SFs
        float eleId(1.);
        float eleReco(1.);
        float eleIso(1.);

        for (const auto* iele : event.m_electrons) {
          eleId   *= iele->isAvailable<float>("EL_SF_ID_"+m_config->electronID()) ? iele->auxdataConst<float>("EL_SF_ID_"+m_config->electronID()) : 1.0;
          eleReco *= iele->isAvailable<float>("EL_SF_Reco") ? iele->auxdataConst<float>("EL_SF_Reco") : 1.;
          eleIso  *= iele->isAvailable<float>("EL_SF_Iso_"+m_config->electronIsolationSF()) ? iele->auxdataConst<float>("EL_SF_Iso_"+m_config->electronIsolationSF()) : 1.0;
        }

        m_electronIdSf   = eleId;
        m_electronRecoSf = eleReco;
        m_electronIsoSf  = eleIso;
        
        float muId(1.);
        float muTTVA(1.);
        float muIso(1.);

        for (const auto* imu : event.m_muons) {
          muId   *= imu->isAvailable<float>("MU_SF_ID_"+m_config->muonQuality()) ? imu->auxdataConst<float>("MU_SF_ID_"+m_config->muonQuality()) : 1.;
          muTTVA *= imu->isAvailable<float>("MU_SF_TTVA") ? imu->auxdataConst<float>("MU_SF_TTVA") : 1.0;
          muIso  *= imu->isAvailable<float>("MU_SF_Isol_"+m_config->muonIsolationSF()) ? imu->auxdataConst<float>("MU_SF_Isol_"+m_config->muonIsolationSF()) : 1.;
        }

        m_muonIdSf   = muId;
        m_muonTTVASf = muTTVA;
        m_muonIsoSf  = muIso;
      }

      ///-- Let the base class do all the hard work --///
      EventSaverFlatNtuple::saveEvent(event);
    }
  }
}
