#ifndef TTBARXSECEVENTSAVER_SINGLELEPSAVER
#define TTBARXSECEVENTSAVER_SINGLELEPSAVER

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopParticleLevel/ParticleLevelEvent.h"

#include <memory>
#include <string>
#include <vector>

/// Forward class declaration
class TFile;
class TopConfig;

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top {
  namespace TTbarXsecEventSaver {
    class SingleLepSaver : public EventSaverFlatNtuple {
    public:
      ///-- Default constrcutor with no arguments - needed for ROOT --///
      explicit SingleLepSaver();
      ///-- Destructor does nothing --///
      virtual ~SingleLepSaver() = default;
      
      ///-- initialize function for top::EventSaverFlatNtuple --///
      ///-- We will be setting up out custom variables here --///
      virtual void initialize(std::shared_ptr<TopConfig> config,
                              TFile* file,
                              const std::vector<std::string>& extraBranches) override;
      
      ///-- Keep the asg::AsgTool happy --///
      virtual StatusCode initialize() override {return StatusCode::SUCCESS;}
      
      ///-- saveEvent function for top::EventSaverFlatNtuple --///
      ///-- We will be setting our custom variables on a per-event basis --///
      virtual void saveEvent(const Event& event) override;
         
    private:
      std::shared_ptr<TopConfig> m_config;
      ///-- Some additional custom variables for the output --///
      bool m_isMC;
      int m_dsid;
      int m_hadBjetIndex;
      int m_lightJetIndex1;
      int m_lightJetIndex2;
      float m_chi2;
      std::vector<int> m_truthJetFlavour;
      float m_electronIdSf;
      float m_electronRecoSf;
      float m_electronIsoSf;
      float m_muonIdSf;
      float m_muonIsoSf;
      float m_muonTTVASf;

      /// Use chi^2 to find the jet to parton matching
      void matchJets(const Event& event);

      /// run single permutation
      void runSinglePermutation(const Event& event, const int bIndex, const int lIndex1, const int lIndex2);

      /// Calculate the chi^2 as (reco_W - m_w)^2/gamma_w + (reco_top - m_top)/gamma_t
      static float chi2(const float recoWmass, const float recoTopMass);
 
      ///-- Tell RootCore to build a dictionary (we need this) --///
      ClassDefOverride(SingleLepSaver, 0);
    };
  }
}

#endif
