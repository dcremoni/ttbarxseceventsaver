#ifndef TTBARXSECEVENTSAVER_DILEPTONSAVER
#define TTBARXSECEVENTSAVER_DILEPTONSAVER

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopParticleLevel/ParticleLevelEvent.h"

#include <memory>
#include <string>
#include <vector>

/// Forward class declaration
class TFile;
class TopConfig;
namespace top {
  class ConfigurationSettings;
}

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top {
  namespace TTbarXsecEventSaver {
    class DileptonSaver : public EventSaverFlatNtuple {
    public:
      ///-- Default constrcutor with no arguments - needed for ROOT --///
      explicit DileptonSaver();
      ///-- Destructor does nothing --///
      virtual ~DileptonSaver() = default;
      
      ///-- initialize function for top::EventSaverFlatNtuple --///
      ///-- We will be setting up out custom variables here --///
      virtual void initialize(std::shared_ptr<TopConfig> config,
                              TFile* file,
                              const std::vector<std::string>& extraBranches) override;
      
      ///-- Keep the asg::AsgTool happy --///
      virtual StatusCode initialize() override {return StatusCode::SUCCESS;}
      
      ///-- saveEvent function for top::EventSaverFlatNtuple --///
      ///-- We will be setting our custom variables on a per-event basis --///
      virtual void saveEvent(const Event& event) override;
         
    private:
      std::shared_ptr<TopConfig> m_config;
      bool m_isMC;
      int m_dsid;
      /*
      float m_dileptonMass;
      float m_dileptonDeltaR;
      float m_dileptonDeltaPhi;
      */
      float m_ee_Mass;
      float m_ee_DeltaR;
      float m_ee_DeltaPhi;
      float m_ee_Z_pt;
      float m_ee_lead_pt;
      float m_ee_subl_pt;
      float m_ee_lead_eta;
      float m_ee_subl_eta;
      float m_ee_lead_phi;
      float m_ee_subl_phi;

      float m_mumu_Mass;
      float m_mumu_DeltaR;
      float m_mumu_DeltaPhi;
      float m_mumu_Z_pt;
      float m_mumu_lead_pt;
      float m_mumu_subl_pt;
      float m_mumu_lead_eta;
      float m_mumu_subl_eta;
      float m_mumu_lead_phi;
      float m_mumu_subl_phi;

      float m_electronIdSf;
      float m_electronRecoSf;
      float m_electronIsoSf;
      float m_muonIdSf;
      float m_muonIsoSf;
      float m_muonTTVASf;
      bool m_storeFtag;
      ConfigurationSettings* m_configSettings;
      ///-- Some additional custom variables for the output --///
 
      ///-- Tell RootCore to build a dictionary (we need this) --///
      ClassDefOverride(DileptonSaver, 0);
    };
  }
}

#endif
