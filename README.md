# TTbarXsecEventSaver [![build status](https://gitlab.cern.ch/tdado/ttbarxseceventsaver/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/tdado/ttbarxseceventsaver/commits/master)

This is a custom event saver that that should be used within AnalysisTop.
It allows to store additional variables used by the Run 3 ttbar inclusive cross-section analyses.
Event savers for lepton+jets and dilepton channels are provided.

## Compilation (assuming you are working from lxplus)
Create a new folder
```
mkdir AT-TTbarXsecEventSaver
cd AT-TTbarXsecEventSaver
```
Create folders
```
mkdir source build run
```
Source configuration
```
setupATLAS
lsetup git
```
Setup AnalysisBase
```
asetup AnalysisBase,master,latest,here # to get nightly
asetup AnalysisBase,22.2.XX,here # to get version XX
cd source/
cp /eos/user/t/topreco/AB_R22_CMakeLists/CMakeLists.txt .
```
Now get the code
```
git clone ssh://git@gitlab.cern.ch:7999/tdado/ttbarxseceventsaver.git
```
Build the code
```
cd ../build/
cmake ../source/
cmake --build ./
source */setup.sh
```
